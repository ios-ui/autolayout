//
//  ALTableViewController.m
//  TSTVariousViewsAutoLayout
//
//  Created by 金鑫 on 2020/5/26.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ALTableViewController.h"
#import "ALCustomCell.h"
#import "ALTextViewCell.h"

@interface ALTableViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *textArray;
@property (nonatomic, strong) NSArray *imageArray;
@end

@implementation ALTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        
        [_tableView registerClass:[ALCustomCell class] forCellReuseIdentifier:@"ALCustomCell"];
        [_tableView registerClass:[ALTextViewCell class] forCellReuseIdentifier:@"ALTextViewCell"];
        
        _tableView.estimatedRowHeight = 40;
        _tableView.rowHeight = UITableViewAutomaticDimension;

    }
    return _tableView;
}

- (NSArray *)textArray {
    if (!_textArray) {
        _textArray = @[@"这里有一个需要特别注意的问题，也是效率问题。UITableView是一次性计算完所有Cell的高度，如果有1W个Cell",@"看了代码，可能你有点疑问，为何这儿要加1呢？笔者告诉你，如果不加1，结果就是错误的，Cell中UILabel将显示不正确。原因就是因为这行代码CGSize size = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];由于是在cell.contentView上调用这个方法，那么返回的值将是contentView的高度，UITableViewCell的高度要比它的contentView要高1,也就是它的分隔线的高度。如果你不相信，那请看C1.xib的属性，比较下面两张图。",@"将Cell中UILabel的背景色",@"我们组分享会上分享了页面布局的一些写法，中途提到了AutoLayout，会后我决定将很久前挖的一个坑给填起来（还有好多坑就不说了，说了不填更毁形象了）",@"Masonry代码如下",@"大叔的话"];
    }
    
    return _textArray;
}

- (NSArray *)imageArray {
    if (!_imageArray) {
        _imageArray = @[@"1.jpg", @"2.jpg", @"3.jpg", @"4.jpg", @"1.jpg", @"2.jpg"];
    }
    return _imageArray;
}

// MARK: -UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return MIN(self.textArray.count, self.imageArray.count) + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    if (indexPath.row == 0) {
        ALTextViewCell *textViewCell = [tableView dequeueReusableCellWithIdentifier:@"ALTextViewCell"];
        cell = textViewCell;
    } else {
        ALCustomCell *customCell = [tableView dequeueReusableCellWithIdentifier:@"ALCustomCell"];
        customCell.textContent = self.textArray[indexPath.row - 1];
        NSString *imagePath = [[NSBundle mainBundle] pathForResource:self.imageArray[indexPath.row - 1] ofType:nil];
        customCell.imageViewRef.image = [UIImage imageWithContentsOfFile:imagePath];
        [customCell.imageViewRef mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo((UIScreen.mainScreen.bounds.size.width - 20) * customCell.imageViewRef.image.size.height / customCell.imageViewRef.image.size.width);
        }];
        cell = customCell;
    }
    
    return cell;
}

@end
