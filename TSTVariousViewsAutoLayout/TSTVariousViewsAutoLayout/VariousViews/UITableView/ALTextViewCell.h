//
//  ALTextViewCell.h
//  TSTVariousViewsAutoLayout
//
//  Created by 金鑫 on 2020/5/26.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ALTextViewCell : UITableViewCell
@property (nonatomic, strong) UITextView *textView;
@end

NS_ASSUME_NONNULL_END
