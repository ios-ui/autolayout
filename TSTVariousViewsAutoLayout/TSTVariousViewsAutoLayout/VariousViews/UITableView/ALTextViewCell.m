//
//  ALTextViewCell.m
//  TSTVariousViewsAutoLayout
//
//  Created by 金鑫 on 2020/5/26.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ALTextViewCell.h"

@interface ALTextViewCell () <UITextViewDelegate>

@end

@implementation ALTextViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.textView];
        __WEAKSELF
        [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(weakSelf.contentView);
        }];
    }
    return self;
}

- (UITextView *)textView {
    if (!_textView) {
        _textView = [[UITextView alloc] init];
        _textView.backgroundColor = [UIColor greenColor];
        _textView.font = [UIFont systemFontOfSize:20];
        _textView.text = @"这里是输入框，可根据输入内容变高";
        _textView.scrollEnabled = NO;
        _textView.delegate = self;
    }
    return _textView;
}

- (void)textViewDidChange:(UITextView *)textView {
    [textView sizeToFit];
    UITableView *tableView = [self tableView];
    [tableView beginUpdates];
    [tableView endUpdates];
}

- (UITableView *)tableView {
    UIView *tableView = self.superview;
    
    while (![tableView isKindOfClass:[UITableView class]] && tableView) {
        tableView = tableView.superview;
    }
    
    return (UITableView *)tableView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
