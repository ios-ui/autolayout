//
//  ALCustomCell.m
//  TSTVariousViewsAutoLayout
//
//  Created by 金鑫 on 2020/5/26.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ALCustomCell.h"


@implementation ALCustomCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.label];
        [self.contentView addSubview:self.imageViewRef];
    }
    
    __WEAKSELF
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-120);
    }];
    
    [self.imageViewRef mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.label.mas_bottom).offset(10);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.bottom.equalTo(weakSelf.contentView.mas_bottom).offset(-10);
    }];
    
    return self;
}

- (UILabel *)label {
    if (!_label) {
        _label = [[UILabel alloc] init];
        _label.backgroundColor = [UIColor redColor];
        _label.numberOfLines = 0;
    }
    return _label;
}

- (UIImageView *)imageViewRef {
    if (!_imageViewRef) {
        _imageViewRef = [[UIImageView alloc] init];
        _imageViewRef.backgroundColor = [UIColor magentaColor];
    }
    return _imageViewRef;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setTextContent:(NSString *)textContent {
    _textContent = textContent;
    
    self.label.text = textContent;
}

@end
