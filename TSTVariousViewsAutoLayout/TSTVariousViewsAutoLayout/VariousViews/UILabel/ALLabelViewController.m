//
//  ALLabelViewController.m
//  TSTVariousViewsAutoLayout
//
//  Created by 金鑫 on 2020/5/25.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ALLabelViewController.h"

@interface ALLabelViewController ()
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *textLabel;
@property (nonatomic, strong) UILabel *attributeLabel;
@end

@implementation ALLabelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.contentView];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.textLabel];
    [self.contentView addSubview:self.attributeLabel];
    [self makeConstraints];
}

- (UIView *)contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] init];
        _contentView.backgroundColor = [UIColor redColor];
    }
    return _contentView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel labelWithText:@"好好学习autolayout你的代码将会更简洁，迭代将会更方便，不信可以试一试" bgColor:[UIColor blueColor] textColor:[UIColor whiteColor] fontSize:20 numberOfLines:0];
    }
    return _titleLabel;
}

- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [UILabel labelWithText:@"还在使用frame？那真是真真是，算了不说了，因为我之前虽然用了约束，但是都是手动算高度，简直浪费了约束这个强大的功能，整天羡慕着安卓的方便，确不知道iOS上也有变高，自动计算高度这东西，而且方便至极！！！！！！" bgColor:[UIColor greenColor] textColor:[UIColor blackColor] fontSize:14 numberOfLines:0];
    }
    return _textLabel;
}

- (UILabel *)attributeLabel {
    if (!_attributeLabel) {
        _attributeLabel = [[UILabel alloc] init];
        _attributeLabel.backgroundColor = [UIColor magentaColor];
        _attributeLabel.numberOfLines = 0;
        NSString *string = @"autolayout对富文本也有很好的支持，它就是自动计算宽高的利器，让开发更简单！你以前对autolayout的使用正确吗？还是说只是把它当作了复杂化的frame呢！！！";
        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:string attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont boldSystemFontOfSize:16]}];
        [attrStr setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:19]} range:[string rangeOfString:@"你以前对autolayout的使用正确吗？"]];
        [attrStr setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:40]} range:[string rangeOfString:@"对富文本也有很好的支持"]];
        
        _attributeLabel.attributedText = attrStr;
    }
    return _attributeLabel;
}

- (void)makeConstraints {
    __WEAKSELF
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(100);
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(-20);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(-20);
    }];
    
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.titleLabel.mas_bottom).offset(40);
        make.left.right.equalTo(weakSelf.titleLabel);
    }];
    
    [self.attributeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.textLabel.mas_bottom).offset(40);
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(-20);
        make.bottom.mas_equalTo(-20);
    }];
}

@end
