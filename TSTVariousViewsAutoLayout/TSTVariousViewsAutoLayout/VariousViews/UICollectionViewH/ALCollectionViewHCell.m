//
//  ALCollectionViewHCell.m
//  TSTVariousViewsAutoLayout
//
//  Created by 金鑫 on 2020/5/28.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ALCollectionViewHCell.h"

@interface ALCollectionViewHCell ()
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UILabel *label;
@end

@implementation ALCollectionViewHCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.containerView];
        [self.containerView addSubview:self.label];
        
        [self makeConstraints];
    }
    return self;
}

- (UIView *)containerView {
    if (!_containerView) {
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor redColor];
        _containerView.layer.cornerRadius = 5;
        _containerView.layer.masksToBounds = YES;
    }
    return _containerView;
}

- (UILabel *)label {
    if (!_label) {
        _label = [UILabel labelWithText:@"" bgColor:[UIColor redColor] textColor:[UIColor whiteColor] fontSize:16 numberOfLines:1];
        _label.textAlignment = NSTextAlignmentCenter;
    }
    return _label;
}

- (void)makeConstraints {
    __WEAKSELF
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(weakSelf.contentView).insets(UIEdgeInsetsMake(5, 0, 5, 0));
    }];
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(weakSelf.containerView).insets(UIEdgeInsetsMake(5, 5, 5, 5));
    }];
}

- (void)loadUIWithTitle:(NSString *)title {
    self.label.text = title;
    [self.label sizeToFit];
    [self layoutIfNeeded];
}
@end
