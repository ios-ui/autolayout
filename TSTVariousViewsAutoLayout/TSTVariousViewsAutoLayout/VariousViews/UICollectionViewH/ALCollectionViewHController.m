//
//  ALCollectionViewHController.m
//  TSTVariousViewsAutoLayout
//
//  Created by 金鑫 on 2020/5/28.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ALCollectionViewHController.h"
#import "ALCollectionViewHCell.h"

@interface ALCollectionViewHController () <UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSArray *titleArray;
@end

@implementation ALCollectionViewHController
static NSString * const ALCollectionViewHCellId = @"ALCollectionViewHCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.titleArray = @[@"战狼",@"北京",@"中华民族",@"大自然",@"这个标题有点长",@"无所谓",@"淡然",@"xcode9.0",@"iOS11",@"iphone x"];
    [self.view addSubview:self.collectionView];
    
    [self makeConstraint];
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(100, 30);
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumInteritemSpacing = 10;
        layout.minimumLineSpacing = 10;
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        _collectionView.backgroundColor = [UIColor cyanColor];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        
        [_collectionView  registerClass:[ALCollectionViewHCell class] forCellWithReuseIdentifier:ALCollectionViewHCellId];
    }
    
    return _collectionView;
}

- (void)makeConstraint {
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(100);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(60);
    }];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.titleArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ALCollectionViewHCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ALCollectionViewHCellId forIndexPath:indexPath];
    [cell loadUIWithTitle:self.titleArray[indexPath.item]];
    
    [cell.contentView sizeToFit];
    
    return cell;
}

@end
