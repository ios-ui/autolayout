//
//  ALCollectionViewHCell.h
//  TSTVariousViewsAutoLayout
//
//  Created by 金鑫 on 2020/5/28.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ALCollectionViewHCell : UICollectionViewCell
- (void)loadUIWithTitle:(NSString *)title;
@end

NS_ASSUME_NONNULL_END
