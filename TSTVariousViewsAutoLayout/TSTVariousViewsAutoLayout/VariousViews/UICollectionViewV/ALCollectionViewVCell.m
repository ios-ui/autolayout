//
//  ALCollectionViewVCell.m
//  TSTVariousViewsAutoLayout
//
//  Created by 金鑫 on 2020/5/28.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ALCollectionViewVCell.h"

@interface ALCollectionViewVCell ()
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *subTitleLabel;
@end

@implementation ALCollectionViewVCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = [UIColor magentaColor];
        [self.contentView addSubview:self.imageView];
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.subTitleLabel];
        
        [self makeConstraints];
    }
    return self;
}

- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.layer.masksToBounds = YES;
    }
    return _imageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel labelWithText:@"" bgColor:[UIColor redColor] textColor:[UIColor whiteColor] fontSize:16 numberOfLines:0];
    }
    return _titleLabel;
}

- (UILabel *)subTitleLabel {
    if (!_subTitleLabel) {
        _subTitleLabel = [UILabel labelWithText:@"" bgColor:[UIColor cyanColor] textColor:[UIColor blackColor] fontSize:13 numberOfLines:0];
    }
    return _subTitleLabel;
}

- (void)makeConstraints {
    
    __WEAKSELF
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(0);
        // FIXME: jinxin 高度写死测试
        make.height.mas_equalTo(120);
        make.width.mas_equalTo(ALScreenW);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.imageView.mas_bottom).offset(10);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
    }];
    
    [self.subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.titleLabel.mas_bottom).offset(5);
        make.left.right.equalTo(weakSelf.titleLabel);
        make.bottom.mas_equalTo(-20);
    }];
}

- (void)loadUIWithImageNamed:(NSString *)imageNamed title:(NSString *)title subTitle:(NSString *)subTitle {
    self.imageView.image = [UIImage imageNamed:imageNamed];
    self.titleLabel.text = title;
    self.subTitleLabel.text = subTitle;
}

@end
