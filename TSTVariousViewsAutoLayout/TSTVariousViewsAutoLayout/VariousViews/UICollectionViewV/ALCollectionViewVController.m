//
//  ALCollectionViewVController.m
//  TSTVariousViewsAutoLayout
//
//  Created by 金鑫 on 2020/5/28.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ALCollectionViewVController.h"
#import "ALCollectionViewVCell.h"

@interface ALCollectionViewVController () <UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSArray *imageArray;
@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, strong) NSArray *subTitleArray;
@end

@implementation ALCollectionViewVController
static NSString * const ALCollectionViewVCellId = @"ALCollectionViewVCell";
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.imageArray = @[@"1.jpg",@"2.jpg",@"3.jpg",@"4.jpg",@"1.jpg",@"2.jpg"];
    self.titleArray = @[@"这里是示例标题",@"这里是示例标题这里是示例标题这里是示例标题这里是示例标题",@"这里是示例标题这里是示例标题这里是示例标题这里是示例标题",@"这里是示例标题这里是示例标题",@"这里是示例标题这里是示例标题这里是示例标题这里是示例标题这里是示例标题这里是示例标题这里是示例标题这里是示例标题这里是示例标题这里是示例标题",@"这里是示例标题"];
    self.subTitleArray = @[@"这里是示例副标题这里是示例副标题这里是示例副标题",@"这里是示例副标题这里是示例副标题这里是示例副标题这里是示例副标题这里是示例副标题这里是示例副标题这里是示例副标题",@"这里是示例副标题这里是示例副标题这里是示例副标题这里是示例副标题这里是示例副标题这里是示例副标题这里是示例副标题这里是示例副标题这里是示例副标题",@"这里是示例副标题这里是示例副标题",@"这里是示例副标题这里是示例副标题这里是示例副标题这里是示例副标题这里是示例副标题这里是示例副标题这里是示例副标题这里是示例副标题",@"这里是示例副标题这里是示例副标题这里是示例副标题这里是示例副标题这里是示例副标题这里是示例副标题"];
    
    [self.view addSubview:self.collectionView];
    
    [self makeContaints];
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.estimatedItemSize = CGSizeMake(1, 1);
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        
        [_collectionView registerClass:[ALCollectionViewVCell class] forCellWithReuseIdentifier:ALCollectionViewVCellId];
    }
    
    return _collectionView;
}

- (void)makeContaints {
    __WEAKSELF
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(weakSelf.view);
    }];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return MIN(MIN(self.titleArray.count, self.subTitleArray.count), self.imageArray.count);
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ALCollectionViewVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ALCollectionViewVCellId forIndexPath:indexPath];
    
    NSInteger index = indexPath.item;
    [cell loadUIWithImageNamed:self.imageArray[index] title:self.titleArray[index] subTitle:self.subTitleArray[index]];
    
    return cell;
}

@end
