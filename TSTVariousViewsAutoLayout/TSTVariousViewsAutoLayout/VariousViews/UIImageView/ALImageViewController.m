//
//  ALImageViewController.m
//  TSTVariousViewsAutoLayout
//
//  Created by 金鑫 on 2020/5/26.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ALImageViewController.h"

@interface ALImageViewController ()
@property (nonatomic, strong) UIImageView *imageView1;
@property (nonatomic, strong) UIImageView *imageView2;

@end

@implementation ALImageViewController

- (UIImageView *)imageView1 {
    if (!_imageView1) {
        NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"1.jpg" ofType:nil];
        UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
        _imageView1 = [[UIImageView alloc] initWithImage:image];
    }
    return _imageView1;
}

- (UIImageView *)imageView2 {
    if (!_imageView2) {
        NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"2.jpg" ofType:nil];
        UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
        _imageView2 = [[UIImageView alloc] initWithImage:image];
    }
    return _imageView2;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];

    [self.view addSubview:self.imageView1];
    [self.view addSubview:self.imageView2];
    
    [self makeContraints];
}

- (void)makeContraints {
    __WEAKSELF
    [self.imageView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(100);
        make.left.mas_equalTo(40);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(200 * weakSelf.imageView1.image.size.height / weakSelf.imageView1.image.size.width);
    }];

    [self.imageView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imageView1.mas_bottom).offset(44);
        make.left.width.equalTo(weakSelf.imageView1);
        make.height.mas_equalTo(200 * weakSelf.imageView2.image.size.height / weakSelf.imageView2.image.size.width);
    }];
    
}

@end
