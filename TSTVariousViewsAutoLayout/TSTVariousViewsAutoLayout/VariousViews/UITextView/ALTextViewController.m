//
//  ALTextViewController.m
//  TSTVariousViewsAutoLayout
//
//  Created by 金鑫 on 2020/5/26.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ALTextViewController.h"

@interface ALTextViewController ()
@property (nonatomic, strong) UITextView *textView;
@end

@implementation ALTextViewController

- (UITextView *)textView {
    if (!_textView) {
        _textView = [[UITextView alloc] init];
        _textView.backgroundColor = [UIColor grayColor];
        _textView.scrollEnabled = NO;
        _textView.font = [UIFont systemFontOfSize:40];
    }
    return _textView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];

    [self.view addSubview:self.textView];

    [self makeConstraints];
    
}

- (void)makeConstraints {
    __WEAKSELF
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(100);
        make.left.right.equalTo(weakSelf.view);
        make.height.greaterThanOrEqualTo(@100);
    }];
}

@end
