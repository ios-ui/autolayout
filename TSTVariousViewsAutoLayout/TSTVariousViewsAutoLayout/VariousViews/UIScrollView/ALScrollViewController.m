//
//  ALScrollViewController.m
//  TSTVariousViewsAutoLayout
//
//  Created by 金鑫 on 2020/5/28.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ALScrollViewController.h"
#import "ALScrollView.h"

@interface ALScrollViewController ()
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) ALScrollView *contentView;
@end

@implementation ALScrollViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.contentView];
    
    [self makeConstraints];
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
    }
    return _scrollView;
}

- (ALScrollView *)contentView {
    if (!_contentView) {
        _contentView = [[ALScrollView alloc] init];
    }
    return _contentView;
}

- (void)makeConstraints {
    __WEAKSELF
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(weakSelf.view).insets(UIEdgeInsetsZero);
//        make.top.left.right.bottom.mas_equalTo(0);
    }];
    
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(weakSelf.scrollView).insets(UIEdgeInsetsZero);
//        make.top.left.right.bottom.mas_equalTo(0);
        make.width.mas_equalTo(weakSelf.scrollView.mas_width);
//        make.width.equalTo(weakSelf.scrollView);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
