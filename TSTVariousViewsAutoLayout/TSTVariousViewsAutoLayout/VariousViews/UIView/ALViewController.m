//
//  ALViewController.m
//  TSTVariousViewsAutoLayout
//
//  Created by 金鑫 on 2020/5/25.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ALViewController.h"

@interface ALViewController ()
@property (nonatomic, strong) UIView *redView;
@property (nonatomic, strong) UIView *greenView;
@property (nonatomic, strong) UIView *blueView;
@property (nonatomic, strong) UIButton *plusButton;
@property (nonatomic, strong) UIButton *minusButton;
@property (nonatomic, assign) NSInteger count;
@end

@implementation ALViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.redView = [self createViewWithColor:[UIColor redColor]];
    self.greenView = [self createViewWithColor:[UIColor greenColor]];
    self.blueView = [self createViewWithColor:[UIColor blueColor]];
    [self.view addSubview:self.redView];
    [self.redView addSubview:self.greenView];
    [self.redView addSubview:self.blueView];
    
    [self.view addSubview:self.plusButton];
    [self.view addSubview:self.minusButton];
    
    [self makeConstraints];
    
}

- (UIButton *)plusButton {
    if (!_plusButton) {
        _plusButton = [UIButton buttonWithTitle:@"plus +" bgColor:[UIColor orangeColor] titleColor:[UIColor whiteColor] titleFont:[UIFont systemFontOfSize:25]];
        [_plusButton addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _plusButton;
}

- (UIButton *)minusButton {
    if (!_minusButton) {
        _minusButton = [UIButton buttonWithTitle:@"minus -" bgColor:[UIColor grayColor] titleColor:[UIColor whiteColor] titleFont:[UIFont systemFontOfSize:25]];
        [_minusButton addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _minusButton;
}

- (void)makeConstraints {
    __WEAKSELF
    [self.redView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(100);
        make.left.mas_equalTo(60);
        make.right.mas_equalTo(-60);
    }];
    
    [self.greenView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.left.mas_equalTo(50);
        make.right.mas_equalTo(-50);
        make.height.mas_equalTo(120);
    }];
    
    [self.blueView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.greenView.mas_bottom).offset(10);
        make.left.mas_equalTo(50);
        make.right.mas_equalTo(-50);
        make.height.mas_equalTo(200);
        make.bottom.equalTo(weakSelf.redView).offset(-40);
    }];
    
    [self.plusButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.redView.mas_bottom).offset(44);
        make.right.mas_equalTo(-40);
        make.height.mas_equalTo(50);
        make.width.mas_equalTo(100);
    }];
    
    [self.minusButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.baseline.equalTo(weakSelf.plusButton);
        make.left.mas_equalTo(40);
        make.height.width.equalTo(weakSelf.plusButton);
    }];
}

- (NSInteger)count {
    if (_count <= 0) {
        return 1;
    }
    
    return _count;
}

- (void)clickButton:(UIButton *)button {
    __WEAKSELF
    
    if (button == self.plusButton) {
        self.count++;
    } else {
        self.count--;
    }
    
    [self.blueView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.greenView.mas_bottom).offset(10 * weakSelf.count);
    }];
    
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (UIView *)createViewWithColor:(UIColor *)color {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = color;
    
    return view;
}


@end
