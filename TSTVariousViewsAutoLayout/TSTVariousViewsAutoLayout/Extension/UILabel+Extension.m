//
//  UILabel+Extension.m
//  TSTVariousViewsAutoLayout
//
//  Created by 金鑫 on 2020/5/25.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "UILabel+Extension.h"

@implementation UILabel (Extension)
+ (instancetype)labelWithText:(NSString *)text bgColor:(UIColor *)bgColor textColor:(UIColor *)textColor fontSize:(CGFloat)fontSize numberOfLines:(NSInteger)lines {
    UILabel *label = [[UILabel alloc] init];
    label.text = text;
    label.backgroundColor = bgColor;
    label.textColor = textColor;
    label.font = [UIFont systemFontOfSize:fontSize];
    label.numberOfLines = 0;
    
    return label;
}
@end
