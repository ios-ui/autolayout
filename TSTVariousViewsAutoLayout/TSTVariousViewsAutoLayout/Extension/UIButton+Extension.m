//
//  UIButton+Extension.m
//  TSTVariousViewsAutoLayout
//
//  Created by 金鑫 on 2020/5/25.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "UIButton+Extension.h"

@implementation UIButton (Extension)

+ (instancetype)buttonWithTitle:(NSString *)title bgColor:(UIColor *)bgColor titleColor:(UIColor *)titleColor titleFont:(UIFont *)font {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    [button setBackgroundColor:bgColor];
    button.titleLabel.font = font;
    
    return button;
}
@end

