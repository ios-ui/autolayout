//
//  UILabel+Extension.h
//  TSTVariousViewsAutoLayout
//
//  Created by 金鑫 on 2020/5/25.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (Extension)
+ (instancetype)labelWithText:(NSString *)text bgColor:(UIColor *)bgColor textColor:(UIColor *)textColor fontSize:(CGFloat)fontSize numberOfLines:(NSInteger)lines;
@end

NS_ASSUME_NONNULL_END
