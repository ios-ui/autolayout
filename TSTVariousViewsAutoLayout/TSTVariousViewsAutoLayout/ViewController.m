//
//  ViewController.m
//  TSTVariousViewsAutoLayout
//
//  Created by 金鑫 on 2020/5/25.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ViewController.h"
#import "ALViewController.h"
#import "ALLabelViewController.h"
#import "ALImageViewController.h"
#import "ALTextViewController.h"
#import "ALTableViewController.h"
#import "ALCollectionViewVController.h"
#import "ALCollectionViewHController.h"
#import "ALScrollViewController.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataArray;
@end

@implementation ViewController
static NSString * const UITableViewCellId = @"UITableViewCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dataArray = @[@"ALViewController",
                       @"ALLabelViewController",
                       @"ALImageViewController",
                       @"ALTextViewController",
                       @"ALTableViewController",
                       @"ALCollectionViewVController",
                       @"ALCollectionViewHController",
                       @"ALScrollViewController"
    ];
    self.navigationController.
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ALScreenW, ALScreenH - 64) style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:UITableViewCellId];
    }
    return _tableView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:UITableViewCellId];
    cell.textLabel.text = self.dataArray[indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *title = self.dataArray[indexPath.row];
    UIViewController *viewController;
    if ([title isEqualToString:@"ALViewController"]) {
        viewController = [[ALViewController alloc] init];
    } else if ([title isEqualToString:@"ALLabelViewController"]) {
        viewController = [[ALLabelViewController alloc] init];
    } else if ([title isEqualToString:@"ALImageViewController"]) {
        viewController = [[ALImageViewController alloc] init];
    } else if ([title isEqualToString:@"ALTextViewController"]) {
        viewController = [[ALTextViewController alloc] init];
    } else if ([title isEqualToString:@"ALTableViewController"]) {
        viewController = [[ALTableViewController alloc] init];
    } else if ([title isEqualToString:@"ALCollectionViewVController"]) {
        viewController = [[ALCollectionViewVController alloc] init];
    } else if ([title isEqualToString:@"ALCollectionViewHController"]) {
        viewController = [[ALCollectionViewHController alloc] init];
    } else if ([title isEqualToString:@"ALScrollViewController"]) {
        viewController = [[ALScrollViewController alloc] init];
    }
    
    viewController.title = title;
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
